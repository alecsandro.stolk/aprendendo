<?php
	include("conexao.php");
	session_start();
	$filtro = "";
	$tentativas = "";
	$desabilita="";
	if(isset($_GET['cod'])){
		$i_palavra = $_GET['cod'];
		//######################## CONTROLE DE TENTATIVAS #####################################
		if (!isset($_SESSION['tentativa'][$i_palavra])){
			$_SESSION['tentativa'][$i_palavra] = 1;
		}
		$_SESSION['tentativa'][$i_palavra]++;
		
		if ($_SESSION['tentativa'][$i_palavra]>=3){
			$desabilita = "display:none";
		}
		//######################## CONTROLE DE TENTATIVAS #####################################
		$filtro = " and i_palavra=$i_palavra";
		$sql = "SELECT a.*,a.descricao as descricao FROM palavras a
			where (select count(*) from palavras b where (b.silaba1=a.silaba1 or b.silaba2=a.silaba2) and b.i_palavra<>a.i_palavra)>0
			and a.silabas >= 3
			$filtro
			order by rand() limit 1";
	}else{
		###################################// CONTROLE  DE PERGUNTAS#############################
		$cont = 0;
		$sql = "SELECT count(*) FROM controle WHERE id ='".$_SESSION['id']."' group by jogo,cod";
		$res = mysqli_query($conexao, $sql);
		while ($rs = mysqli_fetch_array($res)){
			$cont++;
		}
		if ($cont>=10){
			header('Location:resultado.php');
			exit;
		}
		###################################// CONTROLE  DE PERGUNTAS#############################
		$sql = "SELECT a.*,a.descricao as descricao FROM palavras a
			where (select count(*) from palavras b where (b.silaba1=a.silaba1 or b.silaba2=a.silaba2) and b.i_palavra<>a.i_palavra)>0
			and a.silabas >= 3
			and not exists (select 1 from controle b where b.jogo=2 and cod=a.i_palavra and b.id='".$_SESSION['id']."')
			order by rand() limit 1";
	}
	
	// Executa a query
	$res = mysqli_query($conexao, $sql);
	if ($rs = mysqli_fetch_array($res)){
		$cod = $rs['i_palavra'];
		$i_palavra = $rs['i_palavra'];
		$si1 = $rs['silaba1'];
		$si2 = $rs['silaba2'];
		$palavra = $rs['descricao'];
		echo "<audio id='som'>
					<source src='".$rs['audio']."'/>
			  </audio>";
	}else{
		header('Location:resultado.php');
		exit;
	}
	if (!isset($_SESSION['tentativa'][$i_palavra])){
		$_SESSION['tentativa'][$i_palavra] = 1;
	}
	$tentativas = $_SESSION['tentativa'][$i_palavra];
	
?>
<html>
	<head>
		<meta charset="utf-8" />
		<link rel="icon" 
      type="image/png" 
      href="imagens/favicon.png">
		<link rel="stylesheet" href="base.css">
		<link rel="stylesheet" href="layout.css">
		<link rel="stylesheet" href="jquery-gabox-1.1.css">
		<script src="jquery-1.3.2.js" language="javascript"></script>
		<script language="JavaScript">
			function confirmBox() {
				if (confirm("Você deseja sair do jogo?")) {
					location.href="index.html";}
			}
		</script>
		<script>
			function selecionar(cod,ok){
				$("#msg").removeClass();
				for(i=1;i<=4;i++){
					$("#palavra"+i).css('background-color','transparent');
					$("#palavra"+i).css('color','#fff');
				}
				if($("#palavra"+cod).css('background-color')=='transparent'){
					$("#palavra"+cod).css('background-color','#f9edbe');
					$("#palavra"+cod).css('color','#000');
				}else{
					$("#palavra"+cod).css('background-color','transparent');
					$("#palavra"+cod).css('color','#fff');
				}
				$("#block").css('display','block');
				if(ok==0){
					$("#errou").css('display','block');
					$("#acertou").css('display','none');
					errado.play();
				}else{
					$("#acertou").css('display','block');
					$("#errou").css('display','none');
					certo.play();
				}
			}
			function som_palavra(){
				som.play();
			}
			$(document).ready(function(){
				//som.play();
			});
		</script>
		<style type="text/css">
			.botao{
				font-family: Arial;
				border-radius: 10px 10px 10px 10px;
				padding: 5px;
				margin: 5px;
				border: 2px solid #79B8B8;
				cursor: pointer;
				color: #213F7E;
				font-size: 35px;
				float: center;
				width: 50px;
				font-weight: bold;
				background-color: #FFF;
				/*text-transform: uppercase;*/
				
				/*border-radius: 10px 10px 10px 10px;*/
				/*padding: 5px;*/
				/*margin: 5px;*/
				/*border: 1px solid #fff;*/
				/*cursor: pointer;*/
				/*color:#fff;*/
				/*font-size: 30px;*/
				/*background-color:#FFF;*/
			}
			.pontos{
					font-family: Arial;
					border-radius: 10px 10px 10px 10px;
					padding: 5px;
					margin-top: 12px;
					margin-left: 5px;
					border: 2px solid #79B8B8;
					cursor: pointer;
					color: #213F7E;
					font-size: 25px;
					float: left;
					font-weight: bold;
					background-color: #FFF;
					text-transform: uppercase;
			}
			.sair{
				border-radius: 10px 10px 10px 10px;
				padding: 5px;
				margin-center: 0px;
				margin-left: 550px;
				/*border: 1px solid #000;*/
				cursor: pointer;
				color:#FFF;
				font-size: 20px;
				float:center;
				width: 150px;
				background-color:#5077b5;
			}
			.voltar{
				border-radius: 10px 10px 10px 10px;
				padding: 5px;
				margin-center: 0px;
				margin-left: 550px;
				/*border: 1px solid #000;*/
				cursor: pointer;
				color:#FFF;
				font-size: 20px;
				float:center;
				width: 150px;
				background-color:#5077b5;
			}
			.grupo{
				padding: 20px;
				border: 1px solid #f0f;
			}
			#palavra {
				font-family: Arial;
				color:#fffbd8;
				/*padding:10px;*/
				margin-top:170px;
				text-align: center;
				font-size: 50px;
				width:50%;
				font-weight: bold;
				text-shadow: 0.1em 0.06em 0.2em #01061B;
			}
			.form-bg{
				background: url('imagens/fundoimagem.png');
				position: absolute;
				margin-top: 5%;
				margin-left: -360px;
				left: 50%;
				border-radius: 10px 10px 10px 10px;
				background-color: rgba(255, 255, 255, 0);
				width: 707;
				height: 484;
			}
			#acertou{
				border-radius: 10px 10px 10px 10px;
				margin-top: 16px;
				margin-left: 21.5%;
				width: 400px;
				color: #fff;
				background-color:#53a93f;
				display:none;
				z-index:2;
				position:absolute;
			}
			#errou{
				border-radius: 10px 10px 10px 10px;
				margin-top: 16px;
				margin-left: 21.5%;
				width: 400px;
				color: #fff;
				background-color:#FF4F4F;
				display:none;
				z-index:2;
				position:absolute;
			}
			#block{
				position:absolute;
				z-index:1;
				width:100%;
				height:100%;
				opacity:0.75;
				background-color:#FFF;
				display:none;
			}
		</style>
	</head>
	<body style="background:url('imagens/fundo.jpg'); background-position-x: 50%; margin: 0px;">
	
	<div style="margin-left: -360px; left: 50%; width: 707; position: absolute"><img style="position: absolute; padding-top: 10px; padding-left: 50px;" src="imagens/logo2.png"></div>
	
<?php
	//echo "<span class='pontos'> PONTOS: ".$_SESSION[$_SESSION['id']]['pontuacao']."</span>";
	// Verifica a alternativa certa
	$lista = array();
	$sql = "SELECT *,concat(silaba1,silaba2) as descricao FROM palavras where silabas = 2 and silaba1='$si1' and silaba2='$si2' or i_palavra=$cod";
	//$sql = "SELECT *,concat(silaba1,silaba2) as descricao FROM palavras where silabas = 2 and silaba1='$si1' and silaba2='$si2' or i_palavra=$cod";
	//$sql = "SELECT *,descricao as descricao FROM palavras where silaba1='$si1' and silaba2='$si2' or i_palavra=$cod";
	$res = mysqli_query($conexao, $sql);
	if ($rs = mysqli_fetch_array($res)){
		$lista[] = array('descricao' => $rs['descricao'], 'ok' => 1);
		
		$cod2 = $rs['i_palavra'];
	}
	// Seleciona mais três opções para embaralhar
	if($lista){
		$sql = "SELECT descricao as descricao FROM palavras where (i_palavra<>$cod and i_palavra<>$cod2) and (silaba1='$si1' or silaba2='$si2') and silabas = 2 order by rand() limit 3";
		//$sql = "SELECT *,descricao as descricao FROM palavras where i_palavra<>$cod and (silaba1 like '$si1%' or silaba2 like '$si2%') and silabas = 2 order by rand() limit 3";
		//$sql = "SELECT *,descricao as descricao FROM palavras where i_palavra<>$cod and i_palavra<>$cod2 and (silaba1='$si1' or silaba2='$si2') and LENGTH(descricao) <=4 order by rand() limit 3";
		$res = mysqli_query($conexao, $sql);
		while ($rs = mysqli_fetch_array($res)){
			$lista[] = array('descricao' => $rs['descricao'], 'ok' => 0);
		}
	}
	echo "<div id='block' class=''></div>";
		echo "<div class='form-bg' align='center'>";
			echo "<div id='palavra'>".$palavra."</div>";
				echo "<div style='cursor:pointer; margin-top:-131px; float: right; padding-right: 92px;' onclick='som_palavra();'><img src='imagens/audio.png' width='36' height='36' /></div>";
				echo "<br>";
				shuffle($lista);
				$c = 0;
				echo "<div style='margin-top:40px;'>";
				foreach($lista as $l)
				{
					$c++;
					$ok = $l['ok'];
					echo "<span class='botao' id='palavra$c' onclick='selecionar($c,$ok)'>";
							echo $l['descricao'];
							echo "<input type='hidden' name='ok$c' id='ok$c' value='$ok'>";
					echo "</span>";
				}
				echo "</div>";
		
			echo "<div id='acertou'>
					<div><img src='imagens/happy.png' width='70' height='70' /></div>
					PARAB&Eacute;NS VOC&Ecirc; ACERTOU!
					<div><a href='processa.php?cod=$cod&jogo=2&certo=1&tentativas=$tentativas&prox=1' border='0'><img src='imagens/direita.png'  style='width: 48px;'/></a></div>
				 </div>";
					
			echo "<div id='errou'  >
					<div><img src='imagens/sad.png' width='70' height='70' /></div>
					VOC&Ecirc; ERROU!
					<div>
						<a href='processa.php?cod=$cod&jogo=2&certo=0&tentativas=$tentativas&prox=0' border='0' style='".$desabilita."'><img src='imagens/refresh.png'  /></a>
					<a href='processa.php?cod=$cod&jogo=2&certo=0&tentativas=$tentativas&prox=1' border='0'><img src='imagens/direita.png'  style='width: 48px;'/></a>
					</div>
				 </div>";
		echo "</div>";
		//echo "<div class='sair' align='center' style='margin-top:5px;'><a href='index.php'><font color='#FFF'>Sair do jogo</font></a></div>";
?>
	
	<canvas id="tela">Sem suporte ao HTML5</canvas> 
	<audio id="certo">
		<source src="audio/Applause.ogg"/>
		<source src="audio/Applause.mp3"/>
	</audio>
	<audio id="errado">
		<source src="audio/Trompeta.ogg"/>
		<source src="audio/Trompeta.mp3"/>
	</audio>
	<div style="padding-top: 300px;"></div>
	<div align='center' style='margin-top: 70px; padding-top: 20px; position: absolute; left: 50%; margin-left: -45px;'>
	<form>
		<a href='#' onClick='confirmBox()'><img src='imagens/btn_sair.png'></a>
	</form></div>
	</body>
</html>