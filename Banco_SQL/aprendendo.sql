﻿--
-- Banco de Dados: `aprendendo`
--

-- --------------------------------------------------------

DROP TABLE registro;
DROP TABLE controle;
DROP TABLE imagens;
DROP TABLE palavras;
DROP TABLE complete;
DROP TABLE silabas;


--
-- Estrutura da tabela `complete`
--

CREATE TABLE IF NOT EXISTS `complete` (
  `i_palavra` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(250) NOT NULL,
  `letra_correta` varchar(1) NOT NULL,
  `audio` varchar(250) NOT NULL,
  PRIMARY KEY (`i_palavra`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Extraindo dados da tabela `complete`
--

INSERT INTO `complete` (`i_palavra`, `descricao`, `letra_correta`, `audio`) VALUES
(1, 'anheira', 'b', 'audio/banheira.mp3'),
(2, 'oço', 'p', 'audio/poco.mp3'),
(3, 'arraca', 'b', 'audio/barraca.mp3'),
(4, 'erto', 'p', 'audio/perto.mp3'),
(5, 'anela', 'p', 'audio/panela.mp3'),
(6, 'ipa', 'p', 'audio/pipa.mp3'),
(7, 'reto', 'p', 'audio/preto.mp3'),
(8, 'ola', 'b', 'audio/bola.mp3'),
(9, 'otão', 'b', 'audio/botao.mp3'),
(10, 'ule', 'b', 'audio/bule.mp3'),
(11, 'ião', 'p', 'audio/piao.mp3'),
(12, 'rato', 'p', 'audio/prato.mp3'),
(13, 'igode', 'b', 'audio/bigode.mp3'),
(14, 'uraco', 'b', 'audio/buraco.mp3'),
(15, 'raço', 'b', 'audio/braco.mp3'),
(16, 'onito', 'b', 'audio/bonito.mp3'),
(17, 'ota', 'b', 'audio/bota.mp3'),
(18, 'ato', 'p', 'audio/pato.mp3'),
(19, 'atata', 'b', 'audio/batata.mp3'),
(20, 'ássaro', 'p', 'audio/passaro.mp3');

-- --------------------------------------------------------

--
-- Estrutura da tabela `controle`
--

CREATE TABLE IF NOT EXISTS `controle` (
  `i_controle` int(11) NOT NULL AUTO_INCREMENT,
  `jogo` int(11) NOT NULL,
  `cod` int(11) NOT NULL,
  `tentativa` int(11) NOT NULL,
  `certa` int(11) NOT NULL,
  `id` varchar(250) NOT NULL,
  PRIMARY KEY (`i_controle`,`jogo`,`cod`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=789 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `imagens`
--

CREATE TABLE IF NOT EXISTS `imagens` (
  `i_imagem` int(11) NOT NULL AUTO_INCREMENT,
  `endereco` varchar(150) NOT NULL,
  `silaba1` varchar(3) NOT NULL,
  `silaba2` varchar(3) NOT NULL,
  `audio` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`i_imagem`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Extraindo dados da tabela `imagens`
--

INSERT INTO `imagens` (`i_imagem`, `endereco`, `silaba1`, `silaba2`, `audio`) VALUES
(1, 'imagens/casa.png', 'ca', 'sa', 'audio/casa.mp3'),
(2, 'imagens/boca.png', 'bo', 'ca', 'audio/boca.mp3'),
(3, 'imagens/bola.png', 'bo', 'la', 'audio/bola.mp3'),
(4, 'imagens/carro.png', 'car', 'ro', 'audio/carro.mp3'),
(5, 'imagens/chapeu.png', 'cha', 'péu', 'audio/chapeu.mp3'),
(6, 'imagens/seta.png', 'se', 'ta', 'audio/seta.mp3'),
(7, 'imagens/limao.png', 'li', 'mão', 'audio/limao.mp3'),
(8, 'imagens/leao.png', 'le', 'ão', 'audio/leao.mp3'),
(9, 'imagens/maca.png', 'ma', 'çã', 'audio/maca.mp3'),
(10, 'imagens/mesa.png', 'me', 'sa', 'audio/mesa.mp3'),
(11, 'imagens/nuvem.png', 'nu', 'vem', 'audio/nuvem.mp3');

-- --------------------------------------------------------

--
-- Estrutura da tabela `palavras`
--

CREATE TABLE IF NOT EXISTS `palavras` (
  `i_palavra` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(20) NOT NULL,
  `silaba1` varchar(10) NOT NULL,
  `silaba2` varchar(10) NOT NULL,
  `audio` varchar(150) DEFAULT NULL,
  `silabas` int(11) DEFAULT NULL,
  PRIMARY KEY (`i_palavra`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Extraindo dados da tabela `palavras`
--

INSERT INTO `palavras` (`i_palavra`, `descricao`, `silaba1`, `silaba2`, `audio`, `silabas`) VALUES
(1, 'panela', 'pa', 'la', 'audio/panela.mp3', 3),
(2, 'canela', 'ca', 'la', 'audio/canela.mp3', 3),
(3, 'camisa', 'ca', 'sa', 'audio/camisa.mp3', 3),
(4, 'cabeça', 'ca', 'ça', 'audio/cabeca.mp3', 3),
(5, 'palhaço', 'pa', 'ço', 'audio/palhaco.mp3', 3),
(6, 'boneca', 'bo', 'ca', 'audio/boneca.mp3', 3),
(7, 'brinquedo', 'brin', 'do', 'audio/brinquedo.mp3', 3),
(8, 'cavalo', 'ca', 'lo', 'audio/cavalo.mp3', 3),
(9, 'dinossauro', 'di', 'ro', 'audio/dinossauro.mp3', 4),
(10, 'tartaruga', 'tar', 'ga', 'audio/tartaruga.mp3', 4),
(11, 'girafa', 'gi', 'fa', 'audio/girafa.mp3', 3),
(12, 'nela', 'ne', 'la', 'audio/nela.mp3', 2),
(13, 'missa', 'mis', 'sa', 'audio/missa.mp3', 2),
(14, 'capa', 'ca', 'pa', 'audio/capa.mp3', 2),
(15, 'saca', 'sa', 'ca', 'audio/saca.mp3', 2),
(16, 'coisa', 'coi', 'sa', 'audio/coisa.mp3', 2),
(17, 'cabe', 'ca', 'be', 'audio/cabe.mp3', 2),
(18, 'faca', 'fa', 'ca', 'audio/faca.mp3', 2),
(19, 'paco', 'pa', 'co', 'audio/paco.mp3', 2),
(20, 'laço', 'la', 'ço', 'audio/laco.mp3', 2),
(21, 'palha', 'pa', 'lha', 'audio/palha.mp3', 2),
(22, 'aço', 'a', 'ço', 'audio/aco.mp3', 2),
(23, 'seta', 'se', 'ta', 'audio/seta.mp3', 2),
(24, 'beta', 'be', 'ta', 'audio/beta.mp3', 2),
(25, 'beca', 'be', 'ca', 'audio/beca.mp3', 2),
(26, 'cade', 'ca', 'de', 'audio/cade.mp3', 2),
(27, 'bote', 'bo', 'te', 'audio/bote.mp3', 2),
(28, 'beco', 'be', 'co', 'audio/beco.mp3', 2),
(29, 'brinco', 'brin', 'co', 'audio/brinco.mp3', 2),
(30, 'dedo', 'de', 'do', 'audio/dedo.mp3', 2),
(31, 'valo', 'va', 'lo', 'audio/valo.mp3', 2),
(32, 'ruga', 'ru', 'ga', 'audio/ruga.mp3', 2),
(33, 'cabo', 'ca', 'bo', 'audio/cabo.mp3', 2),
(34, 'belo', 'be', 'lo', 'audio/belo.mp3', 2),
(35, 'noro', 'no', 'ro', 'audio/noro.mp3', 2),
(36, 'dino', 'di', 'no', 'audio/dino.mp3', 2),
(37, 'caro', 'ca', 'ro', 'audio/caro.mp3', 2),
(38, 'lapa', 'la', 'pa', 'audio/lapa.mp3', 2),
(39, 'rato', 'ra', 'to', 'audio/rato.mp3', 2),
(40, 'gira', 'gi', 'ra', 'audio/gira.mp3', 2),
(41, 'taco', 'ta', 'co', 'audio/taco.mp3', 2),
(42, 'guga', 'gu', 'ga', 'audio/guga.mp3', 2),
(43, 'medo', 'me', 'do', 'audio/medo.mp3', 2),
(44, 'brilhante', 'bri', 'te', 'audio/brilhante.mp3', 3),
(45, 'serpente', 'ser', 'te', 'audio/serpente.mp3', 3),
(46, 'brinde', 'brin', 'de', 'audio/brinde.mp3', 2),
(47, 'brilho', 'bri', 'lho', 'audio/brilho.mp3', 2),
(48, 'giva', 'gi', 'va', 'audio/giva.mp3', 2),
(49, 'sertão', 'ser', 'tão', 'audio/sertao.mp3', 2),
(50, 'pente', 'pen', 'te', 'audio/pente.mp3', 2),
(51, 'diva', 'di', 'fa', 'audio/diva.mp3', 2),
(52, 'milho', 'mi', 'lho', 'audio/milho.mp3', 2),
(53, 'tarda', 'tar', 'da', 'audio/tarda.mp3', 2),
(54, 'caza', 'ca', 'za', 'audio/caza.mp3', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `registro`
--

CREATE TABLE IF NOT EXISTS `registro` (
  `id` varchar(250) NOT NULL,
  `nome` varchar(250) NOT NULL,
  `jogo` int(11) NOT NULL,
  `pontuacao` decimal(11,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `registro`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `silabas`
--

CREATE TABLE IF NOT EXISTS `silabas` (
  `i_silaba` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`i_silaba`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Extraindo dados da tabela `silabas`
--

INSERT INTO `silabas` (`i_silaba`, `descricao`) VALUES
(1, 'bo'),
(2, 'ca'),
(3, 'sa'),
(4, 'la'),
(5, 'se'),
(6, 'lo'),
(7, 'ba'),
(8, 'si'),
(9, 'mi'),
(10, 'mo'),
(11, 'car'),
(12, 'ro'),
(13, 'cha'),
(14, 'péu'),
(15, 'se'),
(16, 'ta'),
(17, 'li'),
(18, 'mão'),
(19, 'le'),
(20, 'ão'),
(21, 'ma'),
(22, 'me'),
(23, 'nu'),
(24, 'vem'),
(25, 'çã');