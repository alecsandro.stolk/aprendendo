﻿<?php
	header("Content-type: text/html; charset=utf-8");
	include("conexao.php");
	session_start();
	$i_palavra = "";
	$tentativas = "";
	$desabilita="";
	if(isset($_GET['cod'])){
		$i_palavra = $_GET['cod'];
		//######################## CONTROLE DE TENTATIVAS #####################################
		if (!isset($_SESSION['tentativa'][$i_palavra])){
			$_SESSION['tentativa'][$i_palavra] = 1;
		}
		
		$_SESSION['tentativa'][$i_palavra]++;
		
		$desabilita="";
		
		if ($_SESSION['tentativa'][$i_palavra]==3){
			$desabilita = "display:none";
		}
		//######################## CONTROLE DE TENTATIVAS #####################################
		
		$sql = "select * from complete where i_palavra = $i_palavra";
	}else{
		###################################// CONTROLE  DE PERGUNTAS #############################
		$cont = 0;
		$sql = "SELECT count(*) FROM controle WHERE id ='".$_SESSION['id']."' group by jogo,cod";
		$res = mysqli_query($conexao, $sql);
		while ($rs = mysqli_fetch_array($res)){
			$cont++;
		}
		if ($cont>=10){
			header('Location:resultado.php');
			exit;
		}
		###################################// CONTROLE  DE PERGUNTAS #############################
		$sql = "select a.* from complete a
			where not exists (select 1 from controle b where b.jogo=3 and cod=a.i_palavra and b.id='".$_SESSION['id']."')
			order by rand()
			";
	}
	
	$res = mysqli_query($conexao, $sql);
	if ($rs = mysqli_fetch_array($res)){
		$palavra = $rs['descricao'];
		$letra = $rs['letra_correta'];
		$cod = $rs['i_palavra'];
		$i_palavra = $rs['i_palavra'];
		echo "<audio id='som'>
					<source src='".$rs['audio']."'/>
			  </audio>";
	}else{
		header('Location:resultado.php');
		exit;
	}
	if (!isset($_SESSION['tentativa'][$i_palavra])){
		$_SESSION['tentativa'][$i_palavra] = 1;
	}
	$tentativas = $_SESSION['tentativa'][$i_palavra];
	
	
	
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="icon" 
      type="image/png" 
      href="imagens/favicon.png">
		<link rel="stylesheet" href="base.css">
		<link rel="stylesheet" href="layout.css">
		<!--link rel="stylesheet" href="jquery-gabox-1.1.css"-->
		<script src="jquery-3.4.0.js" language="javascript"></script>
<style type="text/css">
			.botao{
				font-family: Arial;
				border-radius: 10px 10px 10px 10px;
				padding: 5px;
				margin: 20px;
				border: 2px solid #79B8B8;
				cursor: pointer;
				color: #213F7E;
				font-size: 50px;
				float: center;
				width: 50px;
				font-weight: bold;
				background-color: #FFF;
				/*text-transform: uppercase;*/
				
				/*border-radius: 10px 10px 10px 10px;*/
				/*padding: 5px;*/
				/*margin: 5px;*/
				/*border: 1px solid #fff;*/
				/*cursor: pointer;*/
				/*color:#fff;*/
				/*font-size: 30px;*/
				/*background-color:#FFF;*/
			}
			.pontos{
					font-family: Arial;
					border-radius: 10px 10px 10px 10px;
					padding: 5px;
					margin: 5px;
					border: 2px solid #79B8B8;
					cursor: pointer;
					color: #213F7E;
					font-size: 25px;
					float: center;
					width: 50px;
					font-weight: bold;
					background-color: #FFF;
					text-transform: uppercase;
			}
			#letra{
				color : #FF0;
			}
			.sair{
				border-radius: 10px 10px 10px 10px;
				padding: 5px;
				margin-center: 0px;
				margin-left: 550px;
				/*border: 1px solid #000;*/
				cursor: pointer;
				color:#FFF;
				font-size: 20px;
				float:center;
				width: 150px;
				background-color:#5077b5;
			}
			.voltar{
				border-radius: 10px 10px 10px 10px;
				padding: 5px;
				margin-center: 0px;
				margin-left: 550px;
				/*border: 1px solid #000;*/
				cursor: pointer;
				color:#FFF;
				font-size: 20px;
				float:center;
				width: 150px;
				background-color:#5077b5;
			}
			.grupo{
				padding: 20px;
				border: 1px solid #f0f;
			}
			#palavra {
				font-family: Arial;
				color:#fffbd8;
				/*padding:10px;*/
				margin-top:100px;
				text-align: center;
				font-size: 50px;
				width:50%;
				font-weight: bold;
				text-shadow: 0.1em 0.06em 0.2em #01061B;
			}
			.form-bg{
				background: url('imagens/fundoimagem.png');
				position: absolute;
				margin-top: 44px;
				margin-left: -360px;
				left: 50%;
				border-radius: 10px 10px 10px 10px;
				background-color: rgba(255, 255, 255, 0);
				width: 707;
				height: 484;
			}
			#acertou{
				border-radius: 10px 10px 10px 10px;
				margin-top: 16px;
				margin-left: 21.5%;
				width: 400px;
				color: #fff;
				background-color:#53a93f;
				display:none;
				z-index:2;
				position:absolute;
			}
			#errou{
				border-radius: 10px 10px 10px 10px;
				margin-top: 16px;
				margin-left: 21.5%;
				width: 400px;
				color: #fff;
				background-color:#FF4F4F;
				display:none;
				z-index:2;
				position:absolute;
			}
			#block{
				position:absolute;
				z-index:1;
				width:100%;
				height:100%;
				opacity:0.75;
				background-color:#FFF;
				display:none;
			}
</style>
		<script>
			function confirmBox() {
				if (confirm("Você deseja sair do jogo?")) {
					location.href="index.html";}
			}
			function selecionar(letra,val){
				$("#letra").html(letra);
				
				$("#block").css('display','block');
				if(val==0){
					$("#errou").css('display','block');
					$("#acertou").css('display','none');
					errado.play();
				}else{
					$("#acertou").css('display','block');
					$("#errou").css('display','none');
					certo.play();
				}
			}
			function som_palavra(){
				som.play();
			}
		</script>
	</head>
	<body style="background:url('imagens/fundo.jpg'); background-position-x: 50%; margin: 0px;">
	<div style="margin-left: -360px; left: 50%; width: 707; position: absolute"><img style="position: absolute; padding-top: 10px; padding-left: 50px;" src="imagens/logo2.png"></div>
<?php	
//echo "<br><span class='pontos'> PONTOS: ".$_SESSION[$_SESSION['id']]['pontuacao']."</span>";
	
	echo "<br>";
		
	$p = 0;
	$b = 0;
	if ($letra=='p'){
		$p = 1;
		$b = 0;
	}else{
		$p = 0;
		$b = 1;
	}
	
	echo "<div id='block' class=''></div>";
	echo "<div class='form-bg' align='center'>";
		echo "<div style='margin-top:100px;'>";
			echo "<span class='botao' id='' onclick='selecionar(\"p\",$p)'>";
									echo "p";
			echo "</span>";
			echo "<span class='botao' id='' onclick='selecionar(\"b\",$b)'>";
									echo "b";
			echo "</span>";
		echo "</div>";
		echo "<div id='palavra'><span id='letra'>...</span>".$palavra."</div>";
		echo "<div style='cursor:pointer; margin-top:-222px; float: right; padding-right: 92px;' onclick='som_palavra();'><img src='imagens/audio.png' width='36' height='36' /></div>";
		
		echo "<div id='acertou'>
				<div><img src='imagens/happy.png' width='70' height='70' /></div>
				PARAB&Eacute;NS VOC&Ecirc; ACERTOU!
				<div><a href='processa.php?cod=$cod&jogo=3&certo=1&tentativas=$tentativas&prox=1' border='0'><img src='imagens/direita.png'  style='width: 48px;'/></a></div>
			 </div>";
				
		echo "<div id='errou'  >
				<div><img src='imagens/sad.png' width='70' height='70' /></div>
				VOC&Ecirc; ERROU!
				<div>
					
					<a href='processa.php?cod=$cod&jogo=3&certo=0&tentativas=$tentativas&prox=0' border='0' style='".$desabilita."'><img src='imagens/refresh.png'  /></a>
					<a href='processa.php?cod=$cod&jogo=3&certo=0&tentativas=$tentativas&prox=1' border='0'><img src='imagens/direita.png'  style='width: 48px;'/></a>
				</div>
				<div></div>
			 </div>";
	echo "</div>";

?>
	<canvas id="tela">Sem suporte ao HTML5</canvas>
	<audio id="certo">
		<source src="audio/Applause.ogg"/>
		<source src="audio/Applause.mp3"/>
	</audio>
	<audio id="errado">
		<source src="audio/Trompeta.ogg"/>
		<source src="audio/Trompeta.mp3"/>
	</audio>
	<div style="padding-top: 300px;"></div>
	<div align='center' style='margin-top: 70px; padding-top: 0px; position: absolute; left: 50%; margin-left: -45px;'>
	<form>
		<a href='#' onClick='confirmBox()'><img src='imagens/btn_sair.png'></a>
	</form></div>
</body>
</html>